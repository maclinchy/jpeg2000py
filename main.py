from tools import *

from PIL import Image

from copy import copy

R_chan, G_chan, B_chan = image_to_RGB('test.png')
Y_chan, U_chan, V_chan = RGB_To_YUV(R_chan, G_chan, B_chan)

w, h = Y_chan.shape[1], Y_chan.shape[0]

new_image = np.zeros((h, w, 3), dtype=np.uint8)


img_Y = copy(new_image)
img_U = copy(new_image)
img_V = copy(new_image)


img_Y[:,:,0] = Y_chan
img_U[:,:,1] = U_chan
img_V[:,:,2] = V_chan


new_image[:,:,0] = Y_chan
new_image[:,:,1] = U_chan
new_image[:,:,2] = V_chan


image_Y = Image.fromarray(img_Y, 'RGB')
image_Y.save('Y.png')

image_U = Image.fromarray(img_U, 'RGB')
image_U.save('U.png')

image_V = Image.fromarray(img_V, 'RGB')
image_V.save('V.png')

image_new = Image.fromarray(new_image, 'RGB')
image_new.save('new.png')

