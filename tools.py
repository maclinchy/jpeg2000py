import numpy as np
import math as m
import imageio


def image_to_RGB(path):
    img = imageio.imread(path)

    R_channel = img[:,:,0]
    G_channel = img[:,:,1]
    B_channel = img[:,:,2]

    return R_channel, G_channel, B_channel


def RGB_To_YUV(R_channel, G_channel, B_channel):
    Y_channel = 0.299*R_channel + 0.587*G_channel + 0.114*B_channel
    U_channel = -0.169*R_channel - 0.331*G_channel + 0.5*B_channel + 128
    V_channel = 0.5*R_channel - 0.419*G_channel - 0.081*B_channel + 128

    return Y_channel, U_channel, V_channel


def gen_dct_matrix(size):

    DCT = np.zeros((size,size))

    for i in range(DCT.shape[0]):
        for j in range(DCT.shape[1]):
            if i == 0:
                DCT[i][j] = 1/m.sqrt(8)
            else:
                DCT[i][j] = m.sqrt(2/8) * m.cos((2*j+1) * i * m.pi/16)
    
    return DCT


def dct_transform(img_matrix):
    return gen_dct_matrix(8)*img_matrix*gen_dct_matrix(8).transpose()


def qvan_matrix(size, qual):
    q_matrix = np.zeros((size, size))

    for i in range(q_matrix.shape[0]):
        for j in range(q_matrix.shape[1]):
            q_matrix[i][j] = 1 + ((1 + i + j) * qual)


    return q_matrix


def qvantion(res_matrix, qvan_matrix):

    for i in range(res_matrix.shape[0]):
        for j in range(res_matrix.shape[1]):
            res_matrix[i][j] /= qvan_matrix[i][j]


    return res_matrix
